# [ElitesAI·动手学深度学习PyTorch版](https://www.boyuai.com/elites/course/cZu18YmweLv10OeV)

## 笔记、习题、参考文献

[Task01：线性回归；Softmax与分类模型、多层感知机](https://gitlab.com/OperaRhino/deep-learning-Pytorch/-/blob/master/ElitesAI-PyTorch/Task01.ipynb)

[Task02：文本预处理；语言模型；循环神经网络基础](https://gitlab.com/OperaRhino/deep-learning-Pytorch/-/blob/master/ElitesAI-PyTorch/Task02.ipynb)

[Task03：过拟合、欠拟合及其解决方案；梯度消失、梯度爆炸；循环神经网络进阶](https://gitlab.com/OperaRhino/deep-learning-Pytorch/-/blob/master/ElitesAI-PyTorch/Task03.ipynb)

[Task04：机器翻译及相关技术；注意力机制与Seq2seq模型；Transformer](https://gitlab.com/OperaRhino/deep-learning-Pytorch/-/blob/master/ElitesAI-PyTorch/Task04.ipynb)

[Task05：卷积神经网络基础；leNet；卷积神经网络进阶](https://gitlab.com/OperaRhino/deep-learning-Pytorch/-/blob/master/ElitesAI-PyTorch/Task05.ipynb)

[Task06：批量归一化和残差网络；凸优化；梯度下降](https://gitlab.com/OperaRhino/deep-learning-Pytorch/-/blob/master/ElitesAI-PyTorch/Task06.ipynb)

[Task07：优化算法进阶；word2vec；词嵌入进阶](https://gitlab.com/OperaRhino/deep-learning-Pytorch/-/blob/master/ElitesAI-PyTorch/Task07.ipynb)

[Task08：文本分类；数据增强；模型微调](https://gitlab.com/OperaRhino/deep-learning-Pytorch/-/blob/master/ElitesAI-PyTorch/Task08.ipynb)

[Task09：目标检测基础；图像风格迁移；图像分类案例1]()

[Task10：图像分类案例2；GAN；DCGAN]()

## 代码

#### Deep Learning:

[线性回归](https://gitlab.com/OperaRhino/deep-learning-Pytorch/-/blob/master/ElitesAI-PyTorch/linear_regression.ipynb),
[Softmax与分类模型](https://gitlab.com/OperaRhino/deep-learning-Pytorch/-/blob/master/ElitesAI-PyTorch/softmax_and_classification.ipynb),
[多层感知机](https://gitlab.com/OperaRhino/deep-learning-Pytorch/-/blob/master/ElitesAI-PyTorch/multilayer_perceptron.ipynb)

[过拟合、欠拟合及其解决方案](https://gitlab.com/OperaRhino/deep-learning-Pytorch/-/blob/master/ElitesAI-PyTorch/overfitting_underfitting_and_solutions.ipynb),
[梯度消失、梯度爆炸](https://gitlab.com/OperaRhino/deep-learning-Pytorch/-/blob/master/ElitesAI-PyTorch/gradient_vanish_and_gradient_explode.ipynb)

[批量归一化和残差网络](https://gitlab.com/OperaRhino/deep-learning-Pytorch/-/blob/master/ElitesAI-PyTorch/batch_normalization_and_residual_networks.ipynb),
[凸优化](https://gitlab.com/OperaRhino/deep-learning-Pytorch/-/blob/master/ElitesAI-PyTorch/convex_optimization.ipynb),
[梯度下降](https://gitlab.com/OperaRhino/deep-learning-Pytorch/-/blob/master/ElitesAI-PyTorch/gradient_descent.ipynb),
[优化算法进阶](https://gitlab.com/OperaRhino/deep-learning-Pytorch/-/blob/master/ElitesAI-PyTorch/advanced_optimization_algorithm.ipynb)

[数据增强](https://gitlab.com/OperaRhino/deep-learning-Pytorch/-/blob/master/ElitesAI-PyTorch/data_augmentation.ipynb),
[模型微调](https://gitlab.com/OperaRhino/deep-learning-Pytorch/-/blob/master/ElitesAI-PyTorch/model_fine-tuning.ipynb)

[循环神经网络基础](https://gitlab.com/OperaRhino/deep-learning-Pytorch/-/blob/master/ElitesAI-PyTorch/recurrent_neural_network.ipynb),
[循环神经网络进阶](https://gitlab.com/OperaRhino/deep-learning-Pytorch/-/blob/master/ElitesAI-PyTorch/modernRNN.ipynb)

[卷积神经网络基础](https://gitlab.com/OperaRhino/deep-learning-Pytorch/-/blob/master/ElitesAI-PyTorch/convolutional_neural_network.ipynb),
[卷积神经网络进阶](https://gitlab.com/OperaRhino/deep-learning-Pytorch/-/blob/master/ElitesAI-PyTorch/modernCNN.ipynb)

#### NLP:
[文本预处理](https://gitlab.com/OperaRhino/deep-learning-Pytorch/-/blob/master/ElitesAI-PyTorch/text_preprocessing.ipynb),
[语言模型](https://gitlab.com/OperaRhino/deep-learning-Pytorch/-/blob/master/ElitesAI-PyTorch/language_models_and_datasets.ipynb),
[注意力机制与Seq2seq模型](https://gitlab.com/OperaRhino/deep-learning-Pytorch/-/blob/master/ElitesAI-PyTorch/attention_mechanism_and_seq2seq_model.ipynb),
[Transformer](https://gitlab.com/OperaRhino/deep-learning-Pytorch/-/blob/master/ElitesAI-PyTorch/transformer.ipynb),
[机器翻译及相关技术](https://gitlab.com/OperaRhino/deep-learning-Pytorch/-/blob/master/ElitesAI-PyTorch/machine_translation.ipynb),
[word2vec](https://gitlab.com/OperaRhino/deep-learning-Pytorch/-/blob/master/ElitesAI-PyTorch/word2vec.ipynb),
[词嵌入进阶](https://gitlab.com/OperaRhino/deep-learning-Pytorch/-/blob/master/ElitesAI-PyTorch/advanced_word_embeddings.ipynb),
[文本分类](https://gitlab.com/OperaRhino/deep-learning-Pytorch/-/blob/master/ElitesAI-PyTorch/text_categorization.ipynb)

#### CV

[leNet](https://gitlab.com/OperaRhino/deep-learning-Pytorch/-/blob/master/ElitesAI-PyTorch/LeNet.ipynb),
[目标检测基础](https://gitlab.com/OperaRhino/deep-learning-Pytorch/-/blob/master/ElitesAI-PyTorch/),
[图像风格迁移](https://gitlab.com/OperaRhino/deep-learning-Pytorch/-/blob/master/ElitesAI-PyTorch/),
[图像分类案例1](https://gitlab.com/OperaRhino/deep-learning-Pytorch/-/blob/master/ElitesAI-PyTorch/),
[图像分类案例2](https://gitlab.com/OperaRhino/deep-learning-Pytorch/-/blob/master/ElitesAI-PyTorch/),
[GAN](https://gitlab.com/OperaRhino/deep-learning-Pytorch/-/blob/master/ElitesAI-PyTorch/),
[DCGAN](https://gitlab.com/OperaRhino/deep-learning-Pytorch/-/blob/master/ElitesAI-PyTorch/)

# Pytorch-Datawhale-8th

